package com.internship.demorestapi.model.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.internship.demorestapi.model.AuthorBookId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@Table(name = "author_book")
@IdClass(AuthorBookId.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AuthorBook implements Serializable {

    @Id
    @Column(name = "author_id")
    private Long authorId;

    @Id
    @Column(name = "book_id")
    private Long bookId;
}
