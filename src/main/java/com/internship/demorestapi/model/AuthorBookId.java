package com.internship.demorestapi.model;

import java.io.Serializable;

public class AuthorBookId implements Serializable{
    Long authorId;
    Long bookId;
}
