package com.internship.demorestapi.repository;

import com.internship.demorestapi.model.entity.AuthorBook;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface AuthorBookRepository extends CrudRepository<AuthorBook, Long> {
}
