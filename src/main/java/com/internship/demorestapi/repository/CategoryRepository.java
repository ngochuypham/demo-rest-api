package com.internship.demorestapi.repository;

import com.internship.demorestapi.model.entity.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
}
