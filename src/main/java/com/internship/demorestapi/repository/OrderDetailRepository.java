package com.internship.demorestapi.repository;

import com.internship.demorestapi.model.entity.OrderDetail;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface OrderDetailRepository extends PagingAndSortingRepository<OrderDetail, Long> {
}
