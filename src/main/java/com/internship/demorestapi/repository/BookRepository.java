package com.internship.demorestapi.repository;

import com.internship.demorestapi.model.entity.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}
